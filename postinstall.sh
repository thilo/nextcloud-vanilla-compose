#!/usr/bin/env bash
set -e
cd /mnt/docker && docker-compose up -d
echo "Getting info from .env file"
ENVFILE="/mnt/docker/.env"
DOMAIN=$(grep ^DOMAIN= "$ENVFILE" | awk -F= '{ print $NF }')
echo "Tweaking nextcloud config"
sed -i "s/localhost/drive.$DOMAIN/g" /mnt/docker/nextcloud/config/config.php
docker exec -ti nextcloud  su - www-data -s /bin/bash -c "php /var/www/html/occ upgrade"
echo "Restarting Nextcloud container"
docker restart nextcloud > /dev/null
