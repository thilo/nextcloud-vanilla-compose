#!/bin/bash
set -e

#source <(curl -s https://gitlab.e.foundation/thilo/bootstrap/raw/master/bootstrap-commons.sh)
source <(curl -s https://gitlab.e.foundation/e/infra/bootstrap/raw/master/bootstrap-commons.sh)

# Create folder structure
cd /mnt/docker && grep mnt docker-compose.yml  | grep -v \# | awk '{ print $2 }' | awk -F: '{ print $1 }' | sed 's@m/.*conf$@m@g'  | grep -v -e id_rsa -v -e exclude_names| sed 's@x/.*conf$@x@g' | sort -u | while read line; do mkdir -p "$line"; done

ENVFILE="/mnt/docker/.env"
rm -f "$ENVFILE"

# Create .env file
generateEnvFile deployment/questionnaire/questionnaire.dat deployment/questionnaire/answers.dat "$ENVFILE"

DOMAIN=$(grep ^DOMAIN= "$ENVFILE" | awk -F= '{ print $NF }')

NEXTCLOUD_ADMIN_USER=$(grep ^NEXTCLOUD_ADMIN_USER= "$ENVFILE" | awk -F= '{ print $NF }')
DBA_USER=$(grep ^DBA_USER= "$ENVFILE" | awk -F= '{ print $NF }')
DBA_PASSWORD=$(grep ^DBA_PASSWORD= "$ENVFILE" | awk -F= '{ print $NF }')
MYSQL_DATABASE_NC=$(grep ^MYSQL_DATABASE_NC= "$ENVFILE" | awk -F= '{ print $NF }')
MYSQL_USER_NC=$(grep ^MYSQL_USER_NC= "$ENVFILE" | awk -F= '{ print $NF }')
MYSQL_PASSWORD_NC=$(grep ^MYSQL_PASSWORD_NC= "$ENVFILE" | awk -F= '{ print $NF }')

# prepare nextcloud DB init scripts
cat /mnt/docker/deployment/ncdb-templates/a_user.sql | sed "s/@@@USER@@@/$MYSQL_USER_NC/g" | sed "s/@@@PASSWORD@@@/$MYSQL_PASSWORD_NC/g" > /mnt/docker/deployment/ncdb/a_user.sql
cat /mnt/docker/deployment/ncdb-templates/b_db.sql | sed "s/@@@ADMINUSER@@@/$NEXTCLOUD_ADMIN_USER/g" | sed "s/@@@DBNAME@@@/$MYSQL_DATABASE_NC/g" > /mnt/docker/deployment/ncdb/b_db.sql
cat /mnt/docker/deployment/ncdb-templates/c_grant.sql | sed "s/@@@USER@@@/$MYSQL_USER_NC/g" | sed "s/@@@DBNAME@@@/$MYSQL_DATABASE_NC/g" > /mnt/docker/deployment/ncdb/c_grant.sql


# generate basic auth for phpmyadmin
htpasswd -c  -b /mnt/docker/nginx/passwds/pma.htpasswd $DBA_USER "$DBA_PASSWORD"

# finished .env file generation

rm -f letsencrypt/autorenew/ssl-domains.dat
# fille autorenew config
echo "$DOMAIN" | tr "," "\n" | while read CURDOMAIN; do
    echo -e "main        $CURDOMAIN\nsub         drive.$CURDOMAIN\nsub         dba.$CURDOMAIN" >> letsencrypt/autorenew/ssl-domains.dat
:; done
cat letsencrypt/autorenew/template-ssl-renew.sh | sed "s/@@@DOMAIN@@@/$DOMAIN/g" > letsencrypt/autorenew/ssl-renew.sh
# Configure nginx vhost
# other hosts
cat nginx/templates/dba | sed "s/@@@DOMAIN@@@/$DOMAIN/g" > "nginx/sites-enabled/dba.$DOMAIN.conf"
cat nginx/templates/drive | sed "s/@@@DOMAIN@@@/$DOMAIN/g" > "nginx/sites-enabled/drive.$DOMAIN.conf"

# confirm DNS is ready
echo ""
echo ""
echo "================================================================================================================================="
echo "================================================================================================================================="
echo "Please setup the following DNS records for your domains before you proceed (subsequent steps will fail if a record is missing):"
echo ""
echo "   $DOMAIN A record to your public IP"
echo "   drive.$DOMAIN CNAME to $DOMAIN."
echo "   dba.$DOMAIN CNAME to $DOMAIN."
echo ""
echo "================================================================================================================================="
echo "================================================================================================================================="
echo ""
echo "Type 'yes' and hit ENTER to confirm that you have setup DNS properly before we continue (everything else will abort the process):"
read CONFIRM < /dev/tty

if [ "yes" != "$CONFIRM" ]
then
    echo "Aborting"
    exit 1
fi

echo "Running LE cert request without verifying DNS setup.."

# Run LE cert request
sh letsencrypt/autorenew/ssl-renew.sh


# verify LE status
CTR_LE=$(find letsencrypt/certstore/live/dba.$DOMAIN/privkey.pem  letsencrypt/certstore/live/drive.$DOMAIN/privkey.pem 2>/dev/null| wc -l)

if [ "$CTR_LE" = "2" ]
then
    echo "All LE certs present."
    echo "Reboot server now and execure postinstall.sh afterwards."
else
    echo "Verification of LE status failed. Some expected certificates are missing"
    echo "$CTR_LE of 2 certificates found."
    exit 1
fi